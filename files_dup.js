var files_dup =
[
    [ "cotask.py", "cotask_8py.html", "cotask_8py" ],
    [ "Lab_1_Vendotron.py", "Lab__1__Vendotron_8py.html", "Lab__1__Vendotron_8py" ],
    [ "Lab_2_Think_Fast.py", "Lab__2__Think__Fast_8py.html", "Lab__2__Think__Fast_8py" ],
    [ "Lab_3_main.py", "Lab__3__main_8py.html", "Lab__3__main_8py" ],
    [ "Lab_3_Spyder.py", "Lab__3__Spyder_8py.html", "Lab__3__Spyder_8py" ],
    [ "Lab_4_main.py", "Lab__4__main_8py.html", "Lab__4__main_8py" ],
    [ "Lab_4_mcp9808.py", "Lab__4__mcp9808_8py.html", [
      [ "TempSensor", "classLab__4__mcp9808_1_1TempSensor.html", "classLab__4__mcp9808_1_1TempSensor" ]
    ] ],
    [ "Lab_6_ODE.py", "Lab__6__ODE_8py.html", "Lab__6__ODE_8py" ],
    [ "Lab_7_TouchPanel.py", "Lab__7__TouchPanel_8py.html", "Lab__7__TouchPanel_8py" ],
    [ "Lab_8_EncoderDriver.py", "Lab__8__EncoderDriver_8py.html", [
      [ "EncoderDriver", "classLab__8__EncoderDriver_1_1EncoderDriver.html", "classLab__8__EncoderDriver_1_1EncoderDriver" ]
    ] ],
    [ "Lab_8_main.py", "Lab__8__main_8py.html", "Lab__8__main_8py" ],
    [ "Lab_8_MotorDriver.py", "Lab__8__MotorDriver_8py.html", [
      [ "MotorDriver", "classLab__8__MotorDriver_1_1MotorDriver.html", "classLab__8__MotorDriver_1_1MotorDriver" ]
    ] ],
    [ "Lab_9_EncoderDriver.py", "Lab__9__EncoderDriver_8py.html", "Lab__9__EncoderDriver_8py" ],
    [ "Lab_9_main.py", "Lab__9__main_8py.html", "Lab__9__main_8py" ],
    [ "Lab_9_MotorDriver.py", "Lab__9__MotorDriver_8py.html", "Lab__9__MotorDriver_8py" ],
    [ "Lab_9_ODE.py", "Lab__9__ODE_8py.html", "Lab__9__ODE_8py" ],
    [ "Lab_9_TouchPanel.py", "Lab__9__TouchPanel_8py.html", [
      [ "TouchPanel", "classLab__9__TouchPanel_1_1TouchPanel.html", "classLab__9__TouchPanel_1_1TouchPanel" ]
    ] ]
];