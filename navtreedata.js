/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "ME 405 Documentation Portfolio", "index.html", [
    [ "Introduction", "index.html#sec_intro", null ],
    [ "Lab Links", "index.html#sec_links", null ],
    [ "Lab 1: Vendotron", "page_V.html", [
      [ "About", "page_V.html#sec_V_a", null ],
      [ "Source Code Access", "page_V.html#sec_V_sca", null ],
      [ "Documentation", "page_V.html#sec_V_doc", null ],
      [ "State Transition Diagrams", "page_V.html#sec_V_STD", null ]
    ] ],
    [ "Lab 2: Think Fast", "page_TF.html", [
      [ "About", "page_TF.html#sec_TF_a", null ],
      [ "Source Code Access", "page_TF.html#sec_TF_sca", null ],
      [ "Documentation", "page_TF.html#sec_TF_doc", null ],
      [ "State Transition Diagrams", "page_TF.html#sec_TF_STD", null ]
    ] ],
    [ "Lab 3: Pushing the Right Buttons", "page_PTRB.html", [
      [ "About", "page_PTRB.html#sec_PTRB_a", null ],
      [ "Source Code Access", "page_PTRB.html#sec_PTRB_sca", null ],
      [ "Documentation", "page_PTRB.html#sec_PTRB_doc", null ],
      [ "State Transition Diagrams", "page_PTRB.html#sec_PTRB_STD", null ],
      [ "Output Graph", "page_PTRB.html#sec_PTRB_OG", null ]
    ] ],
    [ "Lab 4: Hot or Not?", "page_HON.html", [
      [ "About", "page_HON.html#sec_HON_a", null ],
      [ "Source Code Access", "page_HON.html#sec_HON_sca", null ],
      [ "Documentation", "page_HON.html#sec_HON_doc", null ],
      [ "Temperature Graph", "page_HON.html#sec_HON_TG", null ]
    ] ],
    [ "Lab 5: Feeling Tipsy?", "page_FT.html", [
      [ "About", "page_FT.html#sec_FT_a", null ],
      [ "Calculations", "page_FT.html#sec_FT_C", null ]
    ] ],
    [ "Lab 6: Simulation or Reality?", "page_SR.html", [
      [ "About", "page_SR.html#sec_SR_a", null ],
      [ "Source Code Access", "page_SR.html#sec_SR_sca", null ],
      [ "Documentation", "page_SR.html#sec_SR_doc", null ],
      [ "Graphs for different cases", "page_SR.html#sec_SR_3", null ],
      [ "Case a) Ball at Rest at x=0", "page_SR.html#sec_SR_3a", null ],
      [ "Case b) Ball at x = 5cm", "page_SR.html#sec_SR_3b", null ],
      [ "Case c) Platform tilted 5 degrees", "page_SR.html#sec_SR_3c", null ],
      [ "Case d) 1 mN*m*s impulse", "page_SR.html#sec_SR_3d", null ],
      [ "Closed Loop: Case b) Ball at x = 5 cm", "page_SR.html#sec_SR_3b_cl", null ]
    ] ],
    [ "Lab 7: Feeling Touchy", "page_FT2.html", [
      [ "About", "page_FT2.html#sec_FT2_a", null ],
      [ "Source Code Access", "page_FT2.html#sec_FT2_sca", null ],
      [ "Documentation", "page_FT2.html#sec_FT2_doc", null ]
    ] ],
    [ "Lab 8: Term Project Part 1", "page_TP1.html", [
      [ "About", "page_TP1.html#sec_TP1_a", null ],
      [ "Source Code Access", "page_TP1.html#sec_TP1_sca", null ],
      [ "Documentation", "page_TP1.html#sec_TP1_doc", null ]
    ] ],
    [ "Lab 9: Term Project Part 2", "page_TP2.html", [
      [ "About", "page_TP2.html#sec_TP2_a", null ],
      [ "Source Code Access", "page_TP2.html#sec_TP2_sca", null ],
      [ "Documentation", "page_TP2.html#sec_TP2_doc", null ],
      [ "Video", "page_TP2.html#sec_TP2_vid", null ],
      [ "Operating Conditions", "page_TP2.html#sec_TP2_opcon", null ],
      [ "Controller Gain Tuning", "page_TP2.html#sec_TP2_tuning", null ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Functions", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"Lab__1__Vendotron_8py.html",
"classLab__9__MotorDriver_1_1MotorDriver.html#ac14c7927cbd127c4e5c3c5222c55d74d"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';