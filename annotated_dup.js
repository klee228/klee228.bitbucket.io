var annotated_dup =
[
    [ "cotask", null, [
      [ "Task", "classcotask_1_1Task.html", "classcotask_1_1Task" ],
      [ "TaskList", "classcotask_1_1TaskList.html", "classcotask_1_1TaskList" ]
    ] ],
    [ "Lab_1_Vendotron", null, [
      [ "Vendotron", "classLab__1__Vendotron_1_1Vendotron.html", "classLab__1__Vendotron_1_1Vendotron" ]
    ] ],
    [ "Lab_3_Spyder", null, [
      [ "Plotter", "classLab__3__Spyder_1_1Plotter.html", "classLab__3__Spyder_1_1Plotter" ]
    ] ],
    [ "Lab_4_mcp9808", null, [
      [ "TempSensor", "classLab__4__mcp9808_1_1TempSensor.html", "classLab__4__mcp9808_1_1TempSensor" ]
    ] ],
    [ "Lab_7_TouchPanel", null, [
      [ "TouchPanel", "classLab__7__TouchPanel_1_1TouchPanel.html", "classLab__7__TouchPanel_1_1TouchPanel" ]
    ] ],
    [ "Lab_8_EncoderDriver", null, [
      [ "EncoderDriver", "classLab__8__EncoderDriver_1_1EncoderDriver.html", "classLab__8__EncoderDriver_1_1EncoderDriver" ]
    ] ],
    [ "Lab_8_MotorDriver", null, [
      [ "MotorDriver", "classLab__8__MotorDriver_1_1MotorDriver.html", "classLab__8__MotorDriver_1_1MotorDriver" ]
    ] ],
    [ "Lab_9_EncoderDriver", null, [
      [ "EncoderDriver", "classLab__9__EncoderDriver_1_1EncoderDriver.html", "classLab__9__EncoderDriver_1_1EncoderDriver" ]
    ] ],
    [ "Lab_9_MotorDriver", null, [
      [ "MotorDriver", "classLab__9__MotorDriver_1_1MotorDriver.html", "classLab__9__MotorDriver_1_1MotorDriver" ]
    ] ],
    [ "Lab_9_TouchPanel", null, [
      [ "TouchPanel", "classLab__9__TouchPanel_1_1TouchPanel.html", "classLab__9__TouchPanel_1_1TouchPanel" ]
    ] ]
];